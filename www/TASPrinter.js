var exec = require('cordova/exec');

exports.connect = function(success, error) {
    exec(success, error, "TASPrinter", "connect", []);
};

exports.printLine = function(line, fontSize, success, error) {
    exec(success, error, "TASPrinter", "printLine", [line, fontSize]);
};

exports.printImage = function(imageUrl, success, error) {
    exec(success, error, "TASPrinter", "printImage", [imageUrl]);
};

exports.cut = function(success, error) {
    exec(success, error, "TASPrinter", "cut", []);
};

exports.disconnect = function(success, error) {
    exec(success, error, "TASPrinter", "disconnect", []);
};

//
//  PrinterStatus.h
//  CustomiOSApi
//
//  Created by CUSTOM on 08/07/13.
//  Copyright (c) 2013 CUSTOM. All rights reserved.
//

#ifndef PRINTERSTATUS_H_
#define PRINTERSTATUS_H_

#import <Foundation/Foundation.h>

/// <summary>
/// This the container class to hold the printer status
/// </summary>
@interface PrinterStatus : NSObject
{
    @public
    
    @protected
    
    @private
    Boolean statusNOPAPER;
    Boolean statusNEARENDPAP;
    Boolean statusTICKETOUT;
    Boolean statusNOHEAD;
    Boolean statusNOCOVER;
    Boolean statusSPOOLING;
    Boolean statusPAPERROLLING;
    Boolean statusLFPRESSED;
    Boolean statusFFPRESSED;
    Boolean statusOVERTEMP;
    Boolean statusHLVOLT;
    Boolean statusPAPERJAM;
    Boolean statusCUTERROR;
    Boolean statusRAMERROR;
    Boolean statusEEPROMERROR;
}

/// <summary>
/// Create PrinterStatus object with a value
/// </summary>
/// <param name="lStatusPrinter">32bit Status Mask</param>
/// <returns></returns>
-(id)initWithStatus:(long)lStatusPrinter;

/// <summary>
/// Compare PrinterStatus object
/// </summary>
/// <param name="printerstatus">PrinterStatus object</param>
/// <returns></returns>
- (Boolean) IsEqual:(PrinterStatus*) printerstatus;

//**************************************
//Status Vars: BYTE 1
//**************************************

/**
 * Indicates that the printer is in NO PAPER Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsNOPAPER;
/**
 * Indicates that the printer is in NEAR PAPER END Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsNEARENDPAP;
/**
 * Indicates that the printer is in TICKET OUT Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsTICKETOUT;

//**************************************
//Status Vars: BYTE 2
//**************************************

/**
 * Indicates that the printer is in HEAD UP Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsNOHEAD;
/**
 * Indicates that the printer is in COVER OPEN Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsNOCOVER;
/**
 * Indicates that the printer is in SPOOLING Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsSPOOLING;
/**
 * Indicates that the printer is in PAPER ROLLING Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsPAPERROLLING;
/**
 * Indicates that the printer is in LFPRESSED Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsLFPRESSED;
/**
 * Indicates that the printer is in FFPRESSED Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsFFPRESSED;

//**************************************
//Status Vars: BYTE 3
//**************************************

/**
 * Indicates that the printer is in OVERTEMPERATURE Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsOVERTEMP;
/**
 * Indicates that the printer is in OVERVOLTAGE Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsHLVOLT;
/**
 * Indicates that the printer is in PAPER JAM Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsPAPERJAM;

//**************************************
//Status Vars: BYTE 4
//**************************************

/**
 * Indicates that the printer is in CUTTER ERROR Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsCUTERROR;
/**
 * Indicates that the printer is in RAM ERROR Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsRAMERROR;
/**
 * Indicates that the printer is in EEPROM ERROR Status.
 * Refer to Printer manual to know if this status is supported by the device
 */
-(Boolean) StsEEPROMERROR;


@end

#endif

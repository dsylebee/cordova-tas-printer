//
//  CustomiOSApi.h
//  CustomiOSApi
//
//  Created by CUSTOM on 01/07/13.
//  Copyright (c) 2013 CUSTOM. All rights reserved.
//

#ifndef CUSTOMIOSAPI_H_
#define CUSTOMIOSAPI_H_

#import <Foundation/Foundation.h>
#import "CustomPrinter.h"
#import <ExternalAccessory/ExternalAccessory.h>

/// <summary>
/// Factory class to get the printer driver class for supported model id
/// </summary>
@interface CustomiOSApi : NSObject
{
    @public
    
    @protected
    
    @private
}

/// <summary>
/// Returns current API version
/// </summary>
/// <returns>current API version</returns>
- (NSString *) GetAPIVersion;

// <summary>
/// Enable / disable the API log (default disabled)
/// </summary>
/// <param name="enable">Log status</param>
- (void) EnableLogAPI:(Boolean) enable;

//****************************************************************************************************
//****************************************************************************************************
//* WIFI / ETHERNET
//****************************************************************************************************
//****************************************************************************************************

/// <summary>
/// Get the list of the available CUSTOM wifi/ethernet devices
/// </summary>
/// <param name="searchTime">Search wifi/ethernet devices timeout (msec)</param>
/// <param name="error">Error pointer</param>
/// <returns>The array of NSString with IP ADDRESS of CUSTOM wifi/Ethernet devices</returns>
///
/// Example usage:
/// @code
///     //Create the Error Object
///     NSError *error = nil;
///     //Create Main Object
///     CustomiOSApi customApi = [[CustomiOSApi alloc] init];
///     //Enum WIFI devices (search devices for 1.5 secs)
///     NSArray* arrayIP = [customApi EnumWifiDevices:1500 :&error];
///     //Save the 1st IP found
///     NSString* str1stDeviceIP = [arrayIP objectAtIndex:0];
/// @endcode
///
- (NSArray *) EnumWifiDevices:(NSInteger)searchTime :(NSError**)error;

/// <summary>
/// Connects to WiFi/Ethernet Printer device
/// </summary>
/// <param name="printerIpAddr">Printer IP Address</param>
/// <param name="error">Error pointer</param>
/// <returns>Printer driver class according to its model/printer ID</returns>
///
/// Example usage:
/// @code
///     //Create the Error Object
///     NSError *error = nil;
///     //Create Main Object
///     CustomiOSApi customApi = [[CustomiOSApi alloc] init];
///     //Enum WIFI devices (search devices for 1.5 secs)
///     NSArray* arrayIP = [customApi EnumWifiDevices:1500 :&error];
///     //Open the 1st device found (on default 9100 port)
///     CustomPrinter* cp = [customApi GetPrinterDriverWIFI:[arrayIP objectAtIndex:0] :&error];
/// @endcode
///
- (CustomPrinter*) GetPrinterDriverWIFI:(NSString *)printerIpAddr :(NSError**)error;

/// <summary>
/// Connects to WiFi/Ethernet Printer device
/// </summary>
/// <param name="printerIpAddr">Printer IP Address</param>
/// <param name="printerIpPort">Printer IP Port</param>
/// <param name="error">Error pointer</param>
/// <returns>Printer driver class according to its model/printer ID</returns>
///
/// Example usage:
/// @code
///     //Create the Error Object
///     NSError *error = nil;
///     //Create Main Object
///     CustomiOSApi customApi = [[CustomiOSApi alloc] init];
///     //Enum WIFI devices (search devices for 1.5 secs)
///     NSArray* arrayIP = [customApi EnumWifiDevices:1500 :&error];
///     //Open the 1st device found (on 9100 port)
///     CustomPrinter* cp = [customApi GetPrinterDriverWIFI:[arrayIP objectAtIndex:0] :9100 :&error];
/// @endcode
///
- (CustomPrinter*) GetPrinterDriverWIFI:(NSString *)printerIpAddr :(NSInteger)printerIpPort :(NSError**)error;

//****************************************************************************************************
//****************************************************************************************************
//* BLUETOOTH
//****************************************************************************************************
//****************************************************************************************************

/// <summary>
/// Get the list of the available bluetooth devices
/// </summary>
/// <param name="error">Error pointer</param>
/// <returns>The array of EAAccessory of bluetooth devices</returns>
///
/// Example usage:
/// @code
///     //Create the Error Object
///     NSError *error = nil;
///     //Create Main Object
///     CustomiOSApi customApi = [[CustomiOSApi alloc] init];
///     //Enum WIFI devices (search devices)
///     NSArray* arrayBT = [customApi EnumBTDevices:&error];
///     //Save the 1st BT Device found
///     EAAccessory* bt1stDevice = [arrayBT objectAtIndex:0];
/// @endcode
- (NSArray *) EnumBTDevices:(NSError**)error;

/// <summary>
/// Connects to Bluetooth Printer device
/// </summary>
/// <param name="btdevice">EAAccessory* of the device</param>
/// <param name="error">Error pointer</param>
/// <returns>Printer driver class according to its model/printer ID</returns>
///
/// Example usage:
/// @code
///     //Create the Error Object
///     NSError *error = nil;
///     //Create Main Object
///     CustomiOSApi customApi = [[CustomiOSApi alloc] init];
///     //Enum WIFI devices (search devices)
///     NSArray* arrayBT = [customApi EnumBTDevices:&error];
///     //Open the 1st device found
///     CustomPrinter* cp = [customApi GetPrinterDriverBT:[arrayBT objectAtIndex:0] :&error];
/// @endcode
- (CustomPrinter*) GetPrinterDriverBT:(EAAccessory *)btdevice :(NSError**)error;

@end

#endif

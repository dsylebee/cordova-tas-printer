//
//  CustomError.h
//  CustomiOSApi
//
//  Created by CUSTOM on 05/07/13.
//  Copyright (c) 2013 CUSTOM. All rights reserved.
//

#ifndef CUSTOMERROR_H_
#define CUSTOMERROR_H_

#import <Foundation/Foundation.h>

/// <summary>
/// Error class for CeiOSApi
/// </summary>
@interface CustomError : NSObject
{
    @public
    
    /// <summary>
    /// Errors / Warnings Descriptions
    /// </summary>
    enum ErrorCodes
    {
        /**
         * : Indicates that an Generic Error
         */
        ERR_GENERIC                     = 0x00,
        /**
         * : Indicates that an Error on the Picture to print
         */
        ERR_WRONGPICTURE                = 0x01,
        /**
         * : Indicates that an Error on a parameter value
         */
        ERR_WRONGPARAMETERVALUE         = 0x02,
        /**
         * : Indicates that the printer isn't connected
         */
        ERR_PRINTERNOTCONNECTED         = 0x03,
        /**
         * : Indicates that an Error during the communication with the printer
         */
        ERR_PRINTERCOMMUNICATIONERROR   = 0x04,
        /**
         * : Indicates that an Error on the PrinterFont Class
         */
        ERR_WRONGPRINTERFONT            = 0x05,
        /**
         * : Indicates that an Error because this device is not supported
         */
        ERR_DEVICENOTSUPPORTED          = 0x06,
        /**
         * : Indicates that an Error because this device is not recognized
         */
        ERR_DEVICENOTRECOGNIZED         = 0x07,
        /**
         * : Indicates that an Error during the init communication with the printer
         */
        ERR_INITCOMMUNICATIONERROR      = 0x08,
        /**
         * : Indicates that an Error because another operation is in progress
         */
        ERR_OPERATIONINPROGRESS         = 0x09,
        /**
         * : Indicates that an Error during the barcode print
         */
        ERR_DATABARCODE                 = 0x0A,
        /**
         * WARNING: Indicates that this printer do not support this function
         */
        WNG_UNSUPPORTEDFUNCTION         = 0xA1,
    };
    
    @protected

    @private
}

//Hide it in the documentation
//! @cond PRIVATE
+(void) Create:(NSError**)error :(enum ErrorCodes)errcode;
+(void) Create:(NSError**)error :(enum ErrorCodes)errcode :(NSString*)strFormat ,...;
+(void) Init:(NSError**)error;
//! @endcond

@end

#endif

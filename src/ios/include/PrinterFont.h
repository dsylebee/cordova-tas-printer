//
//  PrinterFont.h
//  CustomiOSApi
//
//  Created by CUSTOM on 01/07/13.
//  Copyright (c) 2013 CUSTOM. All rights reserved.
//

#ifndef PRINTERFONT_H_
#define PRINTERFONT_H_

#import <Foundation/Foundation.h>
#import <stdio.h>

/// <summary>
/// This the container class to hold various font attributes
/// </summary>
@interface PrinterFont : NSObject{

    @public
    
    //************************************************************************************************
    // FONT SIZE
    //************************************************************************************************
    
    /// <summary>
    /// Font Size
    /// </summary>
    enum FontSize
    {
        /**
         * Indicates that the Font Size is 1X
         */
        FONT_SIZE_X1                   = 0,
        /**
         * Indicates that the Font Size is 2X
         */
        FONT_SIZE_X2                   = 1,
        /**
         * Indicates that the Font Size is 3X
         */
        FONT_SIZE_X3                   = 2,
        /**
         * Indicates that the Font Size is 4X
         */
        FONT_SIZE_X4                   = 3,
        /**
         * Indicates that the Font Size is 5X
         */
        FONT_SIZE_X5                   = 4,
        /**
         * Indicates that the Font Size is 6X
         */
        FONT_SIZE_X6                   = 5,
        /**
         * Indicates that the Font Size is 7X
         */
        FONT_SIZE_X7                   = 6,
        /**
         * Indicates that the Font Size is 8X
         */
        FONT_SIZE_X8                   = 7,
    };

    //************************************************************************************************
    // FONT JUSTIFICATION
    //************************************************************************************************
    
    /// <summary>
    /// Font Justification
    /// </summary>
    enum FontJustification
    {
        /**
         * Indicates that the Font Justification is Left
         */
        FONT_JUSTIFICATION_LEFT        = 0,
        /**
         * Indicates that the Font Justification is Center
         */
        FONT_JUSTIFICATION_CENTER      = 1,
        /**
         * Indicates that the Font Justification is Right
         */
        FONT_JUSTIFICATION_RIGHT       = 2,
    };
    
    //************************************************************************************************
    // FONT TYPE
    //************************************************************************************************
    
    /// <summary>
    /// Font Type
    /// </summary>
    enum FontType
    {
        /**
         * Indicates that the Font Type is A
         */
        FONT_TYPE_A        			  = 0,
        /**
         * Indicates that the Font Type is B
         */
        FONT_TYPE_B        			  = 1,
    };

    //************************************************************************************************
    // INTERNATIONAL CHARSET
    //************************************************************************************************
    
    /// <summary>
    /// Font International Charset
    /// </summary>
    enum FontIntCharset
    {
        /**
         * Indicates that the International Charset is the Printer Default
         */
        FONT_CS_DEFAULT        		    = 0,
        /**
         * Indicates that the International Charset is Russian (PC866)
         */
        FONT_CS_RUSSIAN        			= 1,
        /**
         * Indicates that the International Charset is Turkish (PC857)
         */
        FONT_CS_TURKISH        			= 2,
        /**
         * Indicates that the International Charset is East Europe (PC852)
         */
        FONT_CS_EASTEEUROPE        		= 3,
        /**
         * Indicates that the International Charset is Israeli (PC862)
         */
        FONT_CS_ISRAELI        			= 4,
        /**
         * Indicates that the International Charset is Greek (PC737)
         */
        FONT_CS_GREEK        			= 5,
    };
    
    @protected

    @private
    
    enum FontSize charWidth;
    enum FontSize charHeight;
    Boolean bemphasized;
    Boolean bitalic;
    Boolean bunderline;
    enum FontJustification justification;
    enum FontType charFontType;
    enum FontIntCharset internationalCharSet;
    NSStringEncoding encodingCharSet;

}

/// <summary>
/// Get Character width ratio (default FONT_SIZE_X1)
/// </summary>
- (enum FontSize) GetCharWidth;

/// <summary>
/// Set Character width ratio
/// </summary>
/// <param name="value">Font width ratio Size</param>
/// <param name="error">Error pointer</param>
- (void) SetCharWidth:(enum FontSize)value :(NSError**)error;

/// <summary>
/// Get Character height ratio (default FONT_SIZE_X1)
/// </summary>
- (enum FontSize) GetCharHeight;

/// <summary>
/// Set Character height ratio
/// </summary>
/// <param name="value">Font height ratio Size</param>
/// <param name="error">Error pointer</param>
- (void) SetCharHeight:(enum FontSize)value :(NSError**)error;

/// <summary>
/// Get Font emphasied (default false)
/// </summary>
- (Boolean) GetEmphasized;

/// <summary>
/// Set Font emphasied
/// </summary>
/// <param name="value">Emphasized value</param>
- (void) SetEmphasized:(Boolean)value;

/// <summary>
/// Get Font italic (default false)
/// </summary>
- (Boolean) GetItalic;

/// <summary>
/// Set Font italic
/// </summary>
/// <param name="value">Italic value</param>
- (void) SetItalic:(Boolean)value;

/// <summary>
/// Get Font underline (default false)
/// </summary>
- (Boolean) GetUnderline;

/// <summary>
/// Set Font underline
/// </summary>
/// <param name="value">Underline value</param>
- (void) SetUnderline:(Boolean)value;

/// <summary>
/// Get Font justification:
/// </summary>
/// <list type="bullet">
/// <item>FONT_JUSTIFICATION_LEFT - left</item>
/// <item>FONT_JUSTIFICATION_CENTER - center</item>
/// <item>FONT_JUSTIFICATION_RIGHT - right</item>
/// </list>
- (enum FontJustification) GetJustification;

/// <summary>
/// Set Font justification:
/// </summary>
/// <param name="value">Justification value:
/// <list type="bullet">
/// <item>FONT_JUSTIFICATION_LEFT - left</item>
/// <item>FONT_JUSTIFICATION_CENTER - center</item>
/// <item>FONT_JUSTIFICATION_RIGHT - right</item>
/// </list>
/// </param>
/// <param name="error">Error pointer</param>
- (void) SetJustification:(enum FontJustification)value :(NSError**)error;

/// <summary>
/// Get Font type:
/// </summary>
/// <list type="bullet">
/// <item>FONT_TYPE_A - Font A.</item>
/// <item>FONT_TYPE_B - Font B.</item>
/// </list>
- (enum FontType) GetCharFontType;

/// <summary>
/// Set Font type:
/// </summary>
/// <param name="value">Font value:
/// <list type="bullet">
/// <item>FONT_TYPE_A - Font A.</item>
/// <item>FONT_TYPE_B - Font B.</item>
/// </list>
/// </param>
/// <param name="error">Error pointer</param>
- (void) SetCharFontType:(enum FontType)value :(NSError**)error;

/// <summary>
/// Get Font international char set:
/// </summary>
/// <list type="bullet">
/// <item>FONT_CS_DEFAULT - Printer Default.</item>
/// <item>FONT_CS_RUSSIAN - RUSSIAN (Only if loaded on printer).</item>
/// <item>FONT_CS_TURKISH - TURKISH (Only if loaded on printer).</item>
/// <item>FONT_CS_EASTEEUROPE - EAST EUROPE (Only if loaded on printer).</item>
/// <item>FONT_CS_ISRAELI - ISRAELI (Only if loaded on printer).</item>
/// <item>FONT_CS_GREEK - GREEK (Only if loaded on printer).</item>
/// </list>
- (enum FontIntCharset) GetInternationalCharSet;

/// <summary>
/// Get Font international char set:
/// </summary>
/// <param name="value">Font international char set value:
/// <list type="bullet">
/// <item>FONT_CS_DEFAULT - Printer Default.</item>
/// <item>FONT_CS_RUSSIAN - RUSSIAN (Only if loaded on printer).</item>
/// <item>FONT_CS_TURKISH - TURKISH (Only if loaded on printer).</item>
/// <item>FONT_CS_EASTEEUROPE - EAST EUROPE (Only if loaded on printer).</item>
/// <item>FONT_CS_ISRAELI - ISRAELI (Only if loaded on printer).</item>
/// <item>FONT_CS_GREEK - GREEK (Only if loaded on printer).</item>
/// </list>
/// </param>
/// <param name="error">Error pointer</param>
- (void) SetInternationalCharSet:(enum FontIntCharset)value :(NSError**)error;

/// <summary>
/// Get Font international char encoding (Only if loaded on printer)
/// </summary>
/// <a href="http://developer.apple.com/library/ios/#documentation/cocoa/reference/foundation/Classes/NSString_Class/Reference/NSString.html%23//apple_ref/occ/clm/NSString/availableStringEncodings">NSStringEncoding references</a>
- (NSStringEncoding) GetEncodingCharSet;

/// <summary>
/// Set Font international char encoding (Only if loaded on printer)
/// </summary>
/// <param name="value">Font international char encoding
/// <a href="http://developer.apple.com/library/ios/#documentation/cocoa/reference/foundation/Classes/NSString_Class/Reference/NSString.html%23//apple_ref/occ/clm/NSString/availableStringEncodings">NSStringEncoding references</a>
/// </param>
/// <param name="error">Error pointer</param>
- (void) SetEncodingCharSet:(NSStringEncoding)value :(NSError**)error;

@end

#endif
//
//  CustomPrinter.h
//  CustomiOSApi
//
//  Created by CUSTOM on 03/07/13.
//  Copyright (c) 2013 CUSTOM. All rights reserved.
//

#ifndef CUSTOMPRINTER_H_
#define CUSTOMPRINTER_H_

#import <Foundation/Foundation.h>
#import "PrinterStatus.h"
#import "PrinterFont.h"
#import <UIKit/UIKit.h>

/// <summary>
/// Class definition for CUSTOM printers. Defines functionality to print text/image, read/write data to printer, etc
/// </summary>
@interface CustomPrinter : NSObject
{
    @public

    /// <summary>
    /// Communication Port Type
    /// </summary>
    enum CommPortType
    {
        /**
         * Indicates that the communication port is a BLUETOOTH
         */
        COMMTYPE_BLUETOOTH		= 0,
        /**
         * Indicates that the communication port is a WIFI
         */
        COMMTYPE_WIFI		    = 1,
        /**
         * Indicates that the communication port isn't init
         */
        COMMTYPE_NONE           = 0xFF,
    };
    
    //************************************************************************************************
    // IMAGE SCALE
    //************************************************************************************************
    
    /// <summary>
    /// Print Image Scale
    /// </summary>
    enum ImageScale
    {
        /**
         * Indicates that the Picture will be No scaling. Excess size will be ignored
         */
        IMAGE_SCALE_NONE           = 0,
        /**
         * Indicates that the Picture will be Scale to fit the paper width
         */
        IMAGE_SCALE_TO_FIT         = 1,
        /**
         * Indicates that the Picture will be Scale the image to width given by width parameter
         */
        IMAGE_SCALE_TO_WIDTH       = 2,
    };
	
    //************************************************************************************************
    // IMAGE ALIGN / JUSTIFICATION
    //************************************************************************************************
    
    /// <summary>
    /// Print Image Justification / Left Position
    /// </summary>
    enum ImageJustification
    {
        /**
         * Indicates that the Picture Justification is Left
         */
        IMAGE_ALIGN_TO_LEFT 	    = 0,
        /**
         * Indicates that the Picture Justification is Center
         */
        IMAGE_ALIGN_TO_CENTER 	    = -1,
        /**
         * Indicates that the Picture Justification is Right
         */
        IMAGE_ALIGN_TO_RIGHT 	    = -2,
    };
    
    //************************************************************************************************
    // CUT
    //************************************************************************************************
    
    /// <summary>
    /// Cut Type
    /// </summary>
    enum CutType
    {
        /**
         * Indicates to perform a Total Cut
         */
        CUT_TOTAL 				    = 0,
        /**
         * Indicates to perform a Partial Cut
         */
        CUT_PARTIAL 			    = 1,
    };
	
    //************************************************************************************************
    // EJECT
    //************************************************************************************************
    
    /// <summary>
    /// Eject Type
    /// </summary>
    enum EjectType
    {
        /**
         * Indicates to perform a Eject or feeds to tear-off postion
         */
        EJ_EJECT 				    = 0,
        /**
         * Indicates to perform a Retract
         */
        EJ_RETRACT 				    = 1,
    };
    
    //************************************************************************************************
    // BARCODE TYPE
    //************************************************************************************************
    
    /// <summary>
    /// Barcode Type
    /// </summary>
    enum BarcodeType
    {
        /**
         * Indicates to print a CODEBAR Barcode
         */
        BARCODE_TYPE_CODABAR                     = -1,
        /**
         * Indicates to print a UPCA Barcode
         */
        BARCODE_TYPE_UPCA                        = -2,
        /**
         * Indicates to print a UPCE Barcode
         */
        BARCODE_TYPE_UPCE                        = -3,
        /**
         * Indicates to print a EAN13 Barcode
         */
        BARCODE_TYPE_EAN13                       = -4,
        /**
         * Indicates to print a EAN8 Barcode
         */
        BARCODE_TYPE_EAN8                        = -5,
        /**
         * Indicates to print a CODE39 Barcode
         */
        BARCODE_TYPE_CODE39                      = -6,
        /**
         * Indicates to print a ITF Barcode
         */
        BARCODE_TYPE_ITF                         = -7,
        /**
         * Indicates to print a CODE93 Barcode
         */
        BARCODE_TYPE_CODE93                      = -8,
        /**
         * Indicates to print a CODE128 Barcode
         */
        BARCODE_TYPE_CODE128                     = -9,
        /**
         * Indicates to print a CODE32 Barcode
         */
        BARCODE_TYPE_CODE32                      = -10,
    };
    
    //************************************************************************************************
    // BARCODE 2D TYPE
    //************************************************************************************************
    
    /// <summary>
    /// Barcode 2D Type
    /// </summary>
    enum Barcode2DType
    {
        /**
         * Indicates to print a QRCODE 2D Barcode
         */
        BARCODE_TYPE_QRCODE                      = -101,
        
        /**
         * Indicates to print a MICROQRCODE 2D Barcode
         */
        BARCODE_TYPE_MICROQRCODE                 = -102,
        
        /**
         * Indicates to print a DATAMATRIX 2D Barcode
         */
        BARCODE_TYPE_DATAMATRIX                  = -103,
        
        /**
         * Indicates to print a PDF417 2D Barcode
         */
        BARCODE_TYPE_PDF417                      = -104,
        
        /**
         * Indicates to print a MICROPDF417 2D Barcode
         */
        BARCODE_TYPE_MICROPDF417                 = -105,
        
        /**
         * Indicates to print a AZTEC 2D Barcode
         */
        BARCODE_TYPE_AZTEC                       = -106,
                
    };
    
    //************************************************************************************************
    // BARCODE HRI TYPE
    //************************************************************************************************
    
    /// <summary>
    /// Barcode HRI Type
    /// </summary>
    enum BarcodeHriType
    {
        /**
         * Indicates no HRI on the Barcode
         */
        BARCODE_HRI_NONE                        = 0,
        /**
         * Indicates HRI on TOP of the Barcode
         */
        BARCODE_HRI_TOP                         = 1,
        /**
         * Indicates HRI on BOTTOM of the Barcode
         */
        BARCODE_HRI_BOTTOM                      = 2,
        /**
         * Indicates HRI on TOP and BOTTOM of the Barcode
         */
        BARCODE_HRI_TOPBOTTOM                   = 3,
    };
    
    //************************************************************************************************
    // BARCODE ALIGN / JUSTIFICATION
    //************************************************************************************************
    
    /// <summary>
    /// Barcode Print Justification / Left Position
    /// </summary>
    enum BarcodeJustification
    {
        /**
         * Indicates that the Barcode Justification is Left
         */
        BARCODE_ALIGN_TO_LEFT = 0,
        /**
         * Indicates that the Barcode Justification is Center
         */
        BARCODE_ALIGN_TO_CENTER = -1,
        /**
         * Indicates that the Barcode Justification is Right
         */
        BARCODE_ALIGN_TO_RIGHT = -2,
    };
    
    @protected

    @private
}

//***********************************************
// COMMON FUNCTIONS
//***********************************************

/// <summary>
/// Returns if the printer is ready or not
/// </summary>
/// <param name="error">Error pointer</param>
/// <returns>Returns if the printer is ready or not</returns>
- (Boolean) IsPrinterOnline:(NSError**)error;

/// <summary>
/// Returns the printer ID
/// </summary>
/// <param name="error">Error pointer</param>
/// <returns>Returns the printer ID</returns>
- (NSInteger) GetPrinterId:(NSError**)error;

/// <summary>
/// Returns the printer FW Release
/// </summary>
/// <param name="error">Error pointer</param>
/// <returns>Returns the printer FW Release</returns>
- (NSString*) GetFirmwareVersion:(NSError**)error;

/// <summary>
/// Returns current API version
/// </summary>
/// <returns>current API version</returns>
- (NSString*) GetAPIVersion;

/// <summary>
/// Returns the status of the printer
/// </summary>
/// <param name="error">Error pointer</param>
/// <returns>Returns the status of the printer. See the printer manual to understand the bit mask</returns>
- (NSData*) GetPrinterFullStatusBuffer:(NSError**)error;

/// <summary>
/// Returns the status of the printer
/// </summary>
/// <param name="error">Error pointer</param>
/// <returns>Returns the status of the printer</returns>
- (PrinterStatus*) GetPrinterFullStatus:(NSError**)error;

/// <summary>
/// Returns name/type of the Printer
/// </summary>
/// <returns>Returns name/type of the Printer</returns>
- (NSString*) GetPrinterName;

/// <summary>
/// Returns the max printable width in pixels / dots
/// </summary>
/// <returns>Returns the max printable width in pixels / dots</returns>
- (NSInteger) GetPageWidth;

/// <summary>
/// Returns the Timeout for Communication Port Read
/// </summary>
/// <param name="error">Error pointer</param>
/// <returns>Timeout for Communication Port Read(msec)</returns>
- (NSInteger) GetReadTimeout:(NSError**)error;

/// <summary>
///  Sets the Timeout for Communication Port Read
/// </summary>
/// <param name="error">Error pointer</param>
/// <param name="msecs">Timeout for Communication Port Read(msec)</param>
- (void) SetReadTimeout:(NSInteger)msecs :(NSError**)error;

/// <summary>
/// Returns the Timeout for Communication Port Write
/// </summary>
/// <param name="error">Error pointer</param>
/// <returns>Timeout for Communication Port Write(msec)</returns>
- (NSInteger) GetWriteTimeout:(NSError**)error;

/// <summary>
/// Sets the Timeout for Communication Port Write
/// </summary>
/// <param name="error">Error pointer</param>
/// <param name="msecs">Timeout for Communication Port Write(msec)</param>
- (void) SetWriteTimeout:(NSInteger)msecs :(NSError**)error;

/// <summary>
/// Disconnect the link, close the communication port and release attached resources
/// </summary>
/// <param name="error">Error pointer</param>
- (void) Close:(NSError**)error;

/// <summary>
/// Returns the communication port type
/// </summary>
/// <returns>Returns the communication port type
/// <list type="bullet">
/// <item>COMMTYPE_NONE for None.</item>
/// <item>COMMTYPE_BLUETOOTH for Bluetooth.</item>
/// <item>COMMTYPE_WIFI for Wifi/Ethernet.</item>
/// </list>
/// </returns>
- (enum CommPortType) GetPortType;

/// <summary>
/// Clears the Read Buffer of Communication Port
/// </summary>
/// <param name="error">Error pointer</param>
- (void) ClearReadBuffer:(NSError**)error;

/// <summary>
/// Read Data Buffer from printer. It will return all available data in buffer
/// </summary>
/// <param name="error">Error pointer</param>
/// <returns>Data Buffer from printer. It will return all available data in buffer</returns>
- (NSData *) ReadData:(NSError**)error;

/// <summary>
/// Write Data Buffer to printer
/// </summary>
/// <param name="error">Error pointer</param>
/// <param name="data">Data Buffer to write to the printer</param>
- (void) WriteData:(NSData *)data :(NSError**)error;

/// <summary>
/// Sends Feeds to the printer
/// </summary>
/// <param name="error">Error pointer</param>
/// <param name="nFeeds">Number of LF to send</param>
- (void) Feed:(NSInteger)nFeeds :(NSError**)error;

/// <summary>
/// Sends a Cut command to the printer
/// </summary>
/// <param name="cutType">Cut Type:
/// <list type="bullet">
/// <item>CUT_TOTAL - Perform a Total Cut.</item>
/// <item>CUT_PARTIAL - Perform a Partial Cut.</item>
/// </list>
/// </param>
/// <param name="error">Error pointer</param>
- (void) Cut:(enum CutType)cutType :(NSError**)error;

/// <summary>
/// Sends a Eject command to the printer
/// </summary>
/// <param name="ejectType">Eject type:
/// <list type="bullet">
/// <item>EJ_EJECT - Perform a Eject or feeds to tear-off postion.</item>
/// <item>EJ_RECTRACT - Perform a Retract.</item>
/// </list>
/// </param>
/// <param name="error">Error pointer</param>
- (void) Eject:(enum EjectType)ejectType :(NSError**)error;

/// <summary>
/// Sends a present command to the printer
/// </summary>
/// <param name="presentMM">mm of paper to present</param>
/// <param name="error">Error pointer</param>
- (void) Present:(NSInteger)presentMM :(NSError**)error;

// <summary>
/// Sends the text data to printer to print
/// </summary>
/// <param name="text">String to print</param>
/// <param name="pixel_la">Left align in pixel</param>
/// <param name="pixel_w">Print area width in pixel. Pass -1 to use max available print width</param>
/// <param name="font">Holds various font attributes</param>
/// <param name="error">Error pointer</param>
- (void) PrintText:(NSString*)text :(NSInteger)pixel_la :(NSInteger)pixel_w :(PrinterFont*)font :(NSError**)error;

/// <summary>
/// Sends the text data to printer to print and adds a LF at the end
/// </summary>
/// <param name="text">String to print</param>
/// <param name="pixel_la">Left align in pixel</param>
/// <param name="pixel_w">Print area width in pixel. Pass -1 to use max available print width</param>
/// <param name="font">Holds various font attributes</param>
/// <param name="error">Error pointer</param>
- (void) PrintTextLF:(NSString*)text :(NSInteger)pixel_la :(NSInteger)pixel_w :(PrinterFont*)font :(NSError**)error;

/// <summary>
/// Sends the text data to printer to print. It will print data on full page width with 0 margin
/// </summary>
/// <param name="text">String to print</param>
/// <param name="font">Holds various font attributes</param>
/// <param name="error">Error pointer</param>
- (void) PrintText:(NSString*)text :(PrinterFont*)font :(NSError**)error;

/// <summary>
/// Sends the text data to printer to print and adds a LF at the end. It will print data on full page width with 0 margin
/// </summary>
/// <param name="text">String to print</param>
/// <param name="font">Holds various font attributes</param>
/// <param name="error">Error pointer</param>
- (void) PrintTextLF:(NSString*)text :(PrinterFont*)font :(NSError**)error;

/// <summary>
/// Sends the text data to printer to print and adds a LF at the end. It will print data on full page width with 0 margin
/// </summary>
/// <param name="textarray">Array of Strings to print</param>
/// <param name="font">Holds various font attributes</param>
/// <param name="error">Error pointer</param>
- (void) PrintTextArrayLF:(NSArray*)textarray :(PrinterFont*)font :(NSError**)error;

/// <summary>
/// Sends the text data to printer to print. It will print data on full page width with 0 margin using the last font attributes
/// </summary>
/// <param name="text">String to print</param>
/// <param name="error">Error pointer</param>
- (void) PrintText:(NSString*)text :(NSError**)error;

/// <summary>
/// Sends the text data to printer to print and adds a LF at the end. It will print data on full page width with 0 margin using the last font attributes
/// </summary>
/// <param name="text">String to print</param>
/// <param name="error">Error pointer</param>
- (void) PrintTextLF:(NSString*)text :(NSError**)error;

/// <summary>
/// Sends the text data to printer to print and adds a LF at the end. It will print data on full page width with 0 margin using the last font attributes
/// </summary>
/// <param name="textarray">Array of Strings to print</param>
/// <param name="error">Error pointer</param>
- (void) PrintTextArrayLF:(NSArray*)textarray :(NSError**)error;

/// <summary>
/// Sends the Bitmap data to printer to print
/// </summary>
/// <param name="image">Bitmap image to print</param>
/// <param name="leftAlign">If the value is > 0, the value is the align Left align in pixel. Otherwise it can be
/// <list type="bullet">
/// <item>IMAGE_ALIGN_TO_LEFT - align the print to the left.</item>
/// <item>IMAGE_ALIGN_TO_CENTER - align the print to the center.</item>
/// <item>IMAGE_ALIGN_TO_RIGHT - align the print to the right.</item>
/// </list>
/// </param>
/// <param name="widthOption">Scaling options:
/// <list type="bullet">
/// <item>IMAGE_SCALE_NONE - No scaling. Excess size will be ignored.</item>
/// <item>IMAGE_SCALE_TO_FIT - Scale to fit the paper width (see GetPageWidth).</item>
/// <item>IMAGE_SCALE_TO_WIDTH - Scale the image to width given by width parameter.</item>
/// </list>
/// </param>
/// <param name="imgWidth">Printed Image width in pixel / dots (used only if widthOption=IMAGE_SCALE_TO_WIDTH)</param>
/// <param name="error">Error pointer</param>
- (void) PrintImage:(UIImage*)image :(NSInteger)leftAlign :(enum ImageScale)widthOption :(NSInteger)imgWidth :(NSError**)error;

/// <summary>
/// Sends the Bitmap data to printer to print fit to paper width
/// </summary>
/// <param name="image">Bitmap image to print</param>
/// <param name="error">Error pointer</param>
- (void) PrintImage:(UIImage*)image :(NSError**)error;

/// <summary>
///  Sends barcode Text to printer to print
/// </summary>
/// <param name="brcText">Barcode Text</param>
/// <param name="brcType">Barcode Type</param>
/// <param name="brcHriType">Barcode HRI (Human Readable Information) Type</param>
/// <param name="brcJustification">Barcode Justification</param>
/// <param name="brcWidth">Barcode Width (referred to the narrow bar)</param>
/// <param name="brcHeight">Barcode Height</param>
/// <param name="error">Error pointer</param>
- (void) PrintBarcodeText:(NSString*)brcText :(enum BarcodeType)brcType :(enum BarcodeHriType)brcHriType :(enum BarcodeJustification)brcJustification :(NSInteger)brcWidth :(NSInteger)brcHeight :(NSError**)error;

/// <summary>
///  Sends barcode data to printer to print
/// </summary>
/// <param name="brcData">Barcode Data</param>
/// <param name="brcType">Barcode Type</param>
/// <param name="brcHriType">Barcode HRI (Human Readable Information) Type</param>
/// <param name="brcJustification">Barcode Justification</param>
/// <param name="brcWidth">Barcode Width (referred to the narrow bar)</param>
/// <param name="brcHeight">Barcode Height</param>
/// <param name="error">Error pointer</param>
- (void) PrintBarcodeData:(NSData*)brcData :(enum BarcodeType)brcType :(enum BarcodeHriType)brcHriType :(enum BarcodeJustification)brcJustification :(NSInteger)brcWidth :(NSInteger)brcHeight :(NSError**)error;

/// <summary>
///  Sends barcode 2D Text to printer to print
/// </summary>
/// <param name="brcText">Barcode Text</param>
/// <param name="brcType">Barcode 2D Type</param>
/// <param name="brcJustification">Barcode Justification</param>
/// <param name="brcWidth">Barcode Width</param>
/// <param name="error">Error pointer</param>
- (void) PrintBarcode2DText:(NSString*)brcText :(enum Barcode2DType)brcType :(enum BarcodeJustification)brcJustification :(NSInteger)brcWidth :(NSError**)error;

/// <summary>
///  Sends barcode 2D data to printer to print
/// </summary>
/// <param name="brcData">Barcode Data</param>
/// <param name="brcType">Barcode 2D Type</param>
/// <param name="brcJustification">Barcode Justification</param>
/// <param name="brcWidth">Barcode Width</param>
/// <param name="error">Error pointer</param>
- (void) PrintBarcode2DData:(NSData*)brcData :(enum Barcode2DType)brcType :(enum BarcodeJustification)brcJustification :(NSInteger)brcWidth :(NSError**)error;

@end

#endif
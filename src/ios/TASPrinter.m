/********* TASPrinter.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import "CustomPrinter.h"
#import "CustomiOSApi.h"

@interface TASPrinter : CDVPlugin {
    
}

- (void)lazyLoadApi;
- (void)lazyLoadPrinter;
- (void)connect:(CDVInvokedUrlCommand*)command;
- (void)printLine:(CDVInvokedUrlCommand*)command;
- (void)printImage:(CDVInvokedUrlCommand*)command;
- (void)cut:(CDVInvokedUrlCommand*)command;
- (void)disconnect:(CDVInvokedUrlCommand*)command;
@end

@implementation TASPrinter {
    CustomiOSApi * _api;
    CustomPrinter * _printer;
}

- (void)lazyLoadApi
{
    if (_api == nil)
        _api = [[CustomiOSApi alloc] init];
}

- (void)lazyLoadPrinter
{
    NSError * error = nil;
    
    // lazy load the api.
    [self lazyLoadApi];
    
    // if there is already a printer just keep exist the method.
    if (_printer != nil)
        return;
    
    // get the list of printers.
    NSArray * bluetoothPrinters = [_api EnumBTDevices:&error];
    if (error == nil)
    {
        if (bluetoothPrinters.count > 0)
        {
            // get the first external accessory (printer)
            EAAccessory * firstEA = [bluetoothPrinters objectAtIndex: 0];
        
            // get the printer from the firstEA
            _printer = [_api GetPrinterDriverBT:firstEA: &error];
        }
    }
}

- (void)connect:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    
    // sets the first printer.
    [self lazyLoadPrinter];

    if (_printer != nil) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)printLine:(CDVInvokedUrlCommand*)command
{
    NSError * error = nil;
    CDVPluginResult* pluginResult = nil;

    // information on what to print.
    NSString * line = [command.arguments objectAtIndex:0];
    NSNumber * fontSize = [command.arguments objectAtIndex:1];

    // we have a printer connected try to print print.
    if (_printer != nil)
        [_printer PrintTextLF:line :&error];
    
    if (_printer == nil || error != nil)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)printImage:(CDVInvokedUrlCommand*)command
{
    NSError * error = nil;
    CDVPluginResult* pluginResult = nil;
    
    // cut the papper.
    if (_printer != nil)
    {
        NSString * dataStr = [command.arguments objectAtIndex:0];
        NSURL *url = [NSURL URLWithString:dataStr];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:imageData];
        [_printer PrintImage:image : &error];
    }
    
    if (_printer == nil || error != nil)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)cut:(CDVInvokedUrlCommand*)command
{
    NSError * error = nil;
    CDVPluginResult* pluginResult = nil;

    // cut the papper.
    if (_printer != nil)
    {
        [_printer Cut:CUT_TOTAL:&error];
    }
    
    if (_printer == nil || error != nil)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)disconnect:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSError * error = nil;
    
    // attempt to close the printer.
    if (_printer != nil)
    {
        [_printer Close:&error];
        
        // if no error during the release of the printer connection.
        if (error == nil)
            _printer = nil;
    }
    
    if (error == nil) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
